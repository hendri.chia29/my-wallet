<?php
/**
 * Encrypts and packs a string.
 * @param string $unPacked unpacked string to pack.
 * @return string packed string.
 */
function packString($unPacked) {
    $pack = @base64_encode(gzdeflate(str_rot13($unPacked)));
    if ($pack == FALSE || empty($pack)) return FALSE;
    return $pack;
}

/**
 * Unpacks a packed string.
 * @param string $packed packed string to unpack.
 * @return string unpacked string.
 */
function unpackString($packed) {
    $packed = str_replace(" ", "+", $packed);
    $unpack = @str_rot13(gzinflate(base64_decode($packed)));
    if ($unpack == FALSE || empty($unpack)) return FALSE;
    return $unpack;
}

/**
 * Pack an array to a encrypted JSON string.
 * @param array $unPacked unpacked array to pack to encrypted JSON string.
 * @return string packed JSON string.
 */
function packJson($unPacked) {
    $pack = packString(json_encode($unPacked));
    if ($pack == FALSE || empty($pack)) return FALSE;
    else return $pack;
}

/**
 * Unpack an encrypted JSON string to its assoc array.
 * @param string $packed packed JSON string to unpack.
 * @return array unpacked assoc array.
 */
function unpackJson($packed) {
    $packed = str_replace(" ", "+", $packed);
    $unpack = json_decode(unpackString($packed), TRUE);
    if ($unpack == FALSE || empty($unpack)) return FALSE;
    else return $unpack;
}

function formatAngka($angka)
{
	return number_format(intval($angka,0),0,"",".");
}

function formatTgl($str)
{
	return date("d M Y",strtotime( $str));
}

function formatTglJam($str)
{
	return date("d M Y H:i:s",strtotime( $str));
}

function getMonthNow()
{
	return date("m");
}

function getYearNow()
{
	return date("Y");
}

function getDateNow()
{
	return date("Y-m-d H:i:s");
}


function round_up($number, $precision = 1)
		{
			$fig = (int) str_pad('1', $precision, '0');
			return (ceil($number * $fig) / $fig);
		}
function encodeUrl($content)
{
		$karakter = array ('{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+','-','/','\\',',','.','#',':',';','\'','"','[',']');
		$hapus_karakter_aneh = strtolower(str_replace($karakter,"",$content));
		$tambah_strip = strtolower(str_replace(' ', '-', $hapus_karakter_aneh));
		$link_akhir = $tambah_strip;
		return $link_akhir;
}
function getCurrentTime()
	{
		$time = date("Y-m-d H:i:s");
		$timestamp = strtotime($time);
		$timestamp_one_hour_later = $timestamp + (3600*7); // 3600 sec. = 1 hour
		
		// Formats the timestamp to HH:MM => outputs 11:09.
		return strftime('%Y-%m-%d %H:%M:%S', $timestamp_one_hour_later);
		// As crolpa suggested, you can also do
		// echo date('H:i', $timestamp_one_hour_later);
	}
?>
