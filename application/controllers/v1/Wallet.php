<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "util_web.php" ;
class Wallet extends CI_Controller {

    private $errMsg="";
    private $succMsg="";
     
	public function __construct()
	{
   			parent::__construct();
            $this->load->helper('url');
            $this->load->library('session');
            $this->load->model('walletmodel/MsCustomerModel',"mscust");
			$this->load->model('walletmodel/WalletHistoryModel',"wallethistory");
			$this->load->model('walletmodel/TokenModel',"tokencust");
            $this->load->helper("security");
            $this->load->database();
	}
	
	function tesToken()
	{
		/*$headers = apache_request_headers();
		$token = $headers['token'];
		die($token);*/
		//die(getAuthTokenHeader());
	}
	
	//get status enable or disable wallet
	function getStatusEnabledWallet($consumerId="")
	{
		$data["id"]=$consumerId;
		$rsCust = $this->mscust->getData($data);
		$isActive=-1;//if data not exists will return -1
		if($rsCust)//if wallet already exists
		{
				foreach ($rsCust as $row){
					$isActive = $row["is_active"];
				}
		}
		return $isActive;
	}
	
	//get consumerId based on header token
	function getConsumerIdByToken($token="")
	{
		$consumerId="-1";
		$data["id"]=$token;
		$rs = $this->tokencust->getData($data);
		$isActive=-1;//if data not exists will return -1
		
		if($rs)//if wallet already exists
		{
				foreach ($rs as $row){
					$consumerId = $row["customer_xid"];
				}
		}
		return $consumerId;
	}
	
	function index()
	{
		$requestMethod 	= $_SERVER["REQUEST_METHOD"];
		$arrResponse	= NULL;
		$token = getAuthTokenHeader();
		$consumerId = $this->getConsumerIdByToken($token);
		$isActive=$this->getStatusEnabledWallet($consumerId);
		if($requestMethod=="POST")//Enable wallet
		{	
			if($isActive==1) //If the wallet is already enabled, this endpoint would fail. This endpint should be usable again only if the wallet is disabled.
			{
				$arrResponse = array(
					"data" => NULL,
					"status" => "failed"
				);
			}
			else if($isActive==-1) //wallet not found
			{
				$arrResponse = array(
					"data" => NULL,
					"status" => "failed"
				);
			}
			else{
					$enabledDatetime = date("Y-m-d H:i:s");
					$data = array();
					$data["enabled_datetime"]=$enabledDatetime;
					$data["is_active"]="1";
					$this->mscust->editData($data,$consumerId);
					$dataResponseGet = array(
							"id"=>create_guid(),
							"owned_by"=>$consumerId,
							"status"=>"enabled",
							"enabled_at"=>$enabledDatetime,
							"balance"=>0
					);
					$dataResponse = array("wallet"=>$dataResponseGet);
					$arrResponse = array(
							"data" => $dataResponse,
							"status" => "success"
					);
			}
		}
		else if($requestMethod=="GET") //View wallet balance
		{
			if($isActive==0) //If the wallet is disabled, this endpoint would fail
			{
				$arrResponse = array(
					"data" => NULL,
					"status" => "failed"
				);
			}
			else{
				$totalWallet = $this->wallethistory->getTotalWallet($consumerId);
				
				$dataCust["id"]=$consumerId;
				$rsCust = $this->mscust->getData($dataCust);
				$enabledDatetime = "";
				if($rsCust)
				{
						foreach ($rsCust as $row){
							$enabledDatetime = $row["enabled_datetime"];
						}
				}
				
				$dataResponseGet = array(
					"id"=>create_guid(),
					"owned_by"=>$consumerId,
					"status"=>"enabled",
					"enabled_at"=>$enabledDatetime,
					"balance"=>$totalWallet
				);
			
				$dataResponse = array("wallet"=>$dataResponseGet);
				$arrResponse = array(
					"data" => $dataResponse, 
					"status" => "success"
				);
			}
		}
		else if($requestMethod=="PATCH") //Disabled wallet
		{
			
			if($isActive==0) //If the wallet is disabled, this endpoint would fail
			{
				$arrResponse = array(
					"data" => NULL , 
					"status" => "failed"
				);
			}
			else{
				$json = file_get_contents('php://input');
				if($json)
				{
					$json_dec 				= json_decode($json);
					$is_disabled = $json_dec->is_disabled;
					
					//disable wallet into Database
					$data = array();
					if($is_disabled==true)
					{
						$data["is_active"]="0";
						$data["disabled_datetime"]=date("Y-m-d H:i:s");
					}
					$this->mscust->editData($data,$consumerId);
					
					//get total balance
					$totalWallet = $this->wallethistory->getTotalWallet($consumerId);
					
					$dataResponseWD = array(
						"id"=>create_guid(),
						"owned_by"=>$consumerId,
						"status"=>"disabled",
						"disabled_at"=>getTimeInISO(),
						"balance"=>$totalWallet
					);
				
					$dataResponse = array("wallet"=>$dataResponseWD);
					$arrResponse = array(
						"data" => $dataResponse, 
						"status" => "success"
					);
				}
			}
		}
		echo printJsonFormat(json_encode($arrResponse));
	}
	
	//deposits
	function deposits()
	{
		$json = file_get_contents('php://input');
		$arrResponse = NULL;
		$dataResponse = NULL;
		
		$token = getAuthTokenHeader();
		$consumerId = $this->getConsumerIdByToken($token);
		$isActive = $this->getStatusEnabledWallet($consumerId);
		if($isActive==0) //If the wallet is disabled, this endpoint would fail
		{
			$arrResponse = array(
					"data" => NULL , 
					"status" => "failed"
			);
		}
		else 
		{
			if($json)
			{
				$json_dec 	= json_decode($json);
				
				$depoAmount = $json_dec->amount;
				$depoRefId = $json_dec->reference_id;
				$depoId = create_guid();
				
				
				$data['amount'] = $depoAmount;
				$data['ref_id'] = $depoRefId;
				$data['history_id']=$depoId;
				$data['customer_xid']=$consumerId;
				$this->wallethistory->addData($data);
				
				
				$dataResponseDeposit = array(
					"id"=>$depoId,
					"deposited_by"=>$consumerId,
					"status"=>"success",
					"deposited_at"=>getTimeInISO(),
					"amount"=>$depoAmount,
					"reference_id"=>$depoRefId
				);
				
				$dataResponse = array("deposit"=>$dataResponseDeposit);
								
				$arrResponse = array(
					"data" => $dataResponse, 
					"status" => "success"
				);
			}
			else
			{
				$dataResponse = array("deposit"=>NULL);
				$arrResponse = array(
					"data" => $dataResponse , 
					"status" => "failed"
				);
			}
		}
		echo printJsonFormat(json_encode($arrResponse));
	}
	
	//withdrawals
	function withdrawals()
	{
		$json = file_get_contents('php://input');
		$arrResponse = NULL;
		$dataResponse = NULL;
		
		$token = getAuthTokenHeader();
		$consumerId = $this->getConsumerIdByToken($token);
		$isActive = $this->getStatusEnabledWallet($consumerId);
		if($isActive==0) //If the wallet is disabled, this endpoint would fail
		{
			$arrResponse = array(
					"data" => NULL , 
					"status" => "failed"
			);
		}
		else
		{
			if($json)
			{
				$json_dec 				= json_decode($json);
				
				$wdAmount = $json_dec->amount;
				$wdRefId = $json_dec->reference_id;
				$wdId = create_guid();
				
				$data['amount'] = $wdAmount*-1;
				$data['ref_id'] = $wdRefId;
				$data['history_id']=$wdId;
				$data['customer_xid']=$consumerId;
				$this->wallethistory->addData($data);
							
				$dataResponseWD = array(
					"id"=>$wdId,
					"withdrawn_by"=>$consumerId,
					"status"=>"success",
					"withdrawn_at"=>getTimeInISO(),
					"amount"=>$wdAmount,
					"reference_id"=>$wdRefId
				);
				
				$dataResponse = array("withdrawal"=>$dataResponseWD);
				$arrResponse = array(
					"data" => $dataResponse, 
					"status" => "success"
				);
			}
			else
			{
				$dataResponse = array("withdrawal"=>NULL);
				$arrResponse = array(
					"data" => $dataResponse , 
					"status" => "failed"
				);
			}
		}
		echo printJsonFormat(json_encode($arrResponse));	
	}
}
?>