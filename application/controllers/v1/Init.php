<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "util_web.php" ;
class Init extends CI_Controller {

    private $errMsg="";
    private $succMsg="";
     
	public function __construct()
	{
   			parent::__construct();
            $this->load->helper('url');
            $this->load->library('session');
            $this->load->model('walletmodel/MsCustomerModel',"mscust");
			$this->load->model('walletmodel/WalletHistoryModel',"wallethistory");
			$this->load->model('walletmodel/TokenModel',"tokencust");
            $this->load->helper("security");
            $this->load->database();
	}
	
	//Initialize account for wallet
	function index(){
		$json = file_get_contents('php://input');
		$arrResponse = NULL;
		if($json)
		{
			$consumerId="";
			$json_dec 				= json_decode($json);
			$consumerId 			= $json_dec->customer_xid;
			
			$dataCek=array("id"=> $consumerId);
			
			//cek is consumerId is exists
			$rsCust = $this->mscust->isDataExists($dataCek);
			
			if($rsCust==0)
			{
				
				//save customer data
				$data['customer_xid'] 	= $consumerId;
				$data['is_active'] 		= 0;
				$this->mscust->addData($data);
			
				//generate token for wallet
				$token = create_guid();
				
				//save token
				$dataT=array();
				$dataT['customer_xid'] 	= $consumerId;
				$dataT['tokens'] 		= $token;
				$this->tokencust->addData($dataT);
				
				
				$dataRes = array ("token" => $token);
				$arrResponse = array(
					"data" => $dataRes, 
					"status" => "success"
				);
			}
			else{ //if consumerid already exists in database
				$arrResponse = array(
					"data" => NULL, 
					"status" => "failed"
				);
			}
		}
		else
		{
			$arrResponse = array(
				"data" => NULL , 
				"status" => "failed"
			);
		}
		echo printJsonFormat(json_encode($arrResponse));
	}
}
?>