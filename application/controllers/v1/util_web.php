<?php
//	session_start();
  	define('STICKER_IMG_DIR', 'resource/sticker_images/');
	define('PRODUCT_IMG_DIR', 'resource/product_images/');
	define('BG_DIR', 'resource/bg_images/');
	define('LAYOUT_DIR', 'resource/layout/');
	define('PHOTO_DIR', 'resource/user_images/');
	define('CURRENCY', '$');
	function packString($decString) {
		$ret = base64_encode(gzdeflate(str_rot13($decString)));
		if ($ret !== FALSE) {
			$ret = str_replace (array("+", "/"), array("-", "_"), $ret);
			$ret = str_replace ("=", "", $ret);
			return $ret;
		} //return $ret;
		else return FALSE;
	}

	function unpackString($encString) {
		$encString = str_replace(array("-", "_"), array("+", "/"), $encString);
		$test = base64_decode($encString);
		if ($test == FALSE) return FALSE;
		else return str_rot13(gzinflate($test));
	}
	function sanitize($someString)
	{
		return htmlspecialchars(stripslashes(trim($someString)), ENT_QUOTES);
	}
	function isValidEmail($email)
	{
		return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $email);
	}
	function printJsonFormat($json) 
	{ 
		$tab = "  "; 
		$new_json = ""; 
		$indent_level = 0; 
		$in_string = false; 
	
		$json_obj = json_decode($json); 
	
		if($json_obj === false) 
			return false; 
	
		$json = json_encode($json_obj); 
		$len = strlen($json); 
	
		for($c = 0; $c < $len; $c++) 
		{ 
			$char = $json[$c]; 
			switch($char) 
			{ 
				case '{': 
				case '[': 
					if(!$in_string) 
					{ 
						$new_json .= $char . "\n" . str_repeat($tab, $indent_level+1); 
						$indent_level++; 
					} 
					else 
					{ 
						$new_json .= $char; 
					} 
					break; 
				case '}': 
				case ']': 
					if(!$in_string) 
					{ 
						$indent_level--; 
						$new_json .= "\n" . str_repeat($tab, $indent_level) . $char; 
					} 
					else 
					{ 
						$new_json .= $char; 
					} 
					break; 
				case ',': 
					if(!$in_string) 
					{ 
						$new_json .= ",\n" . str_repeat($tab, $indent_level); 
					} 
					else 
					{ 
						$new_json .= $char; 
					} 
					break; 
				case ':': 
					if(!$in_string) 
					{ 
						$new_json .= ": "; 
					} 
					else 
					{ 
						$new_json .= $char; 
					} 
					break; 
				case '"': 
					if($c > 0 && $json[$c-1] != '\\') 
					{ 
						$in_string = !$in_string; 
					} 
				default: 
					$new_json .= $char; 
					break;                    
			} 
		} 	
		
		//return "<pre>".$new_json."</pre>"; 
		return $new_json; 
	}
	
	function getCurrentDate()
	{
		return date("Y-m-d");
	}
	
	function getCurrentDateAndTime()
	{
		return date("Y-m-d H:i:s");
	}
	
	function create_guid(){
		$guid="";
		$namespace = rand(11111, 99999);$uid=uniqid("",true);
		$data=$namespace;
		$data .= $_SERVER['REQUEST_TIME'];
		$data .= $_SERVER['HTTP_USER_AGENT'];
		$data .= $_SERVER['REMOTE_ADDR'];
		$data .= $_SERVER['REMOTE_PORT'];
		$hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
		$guid = substr($hash,0,8). '-' .substr($hash,8,4) . '-' .substr($hash,12,4) . '-' .substr($hash,16,4) . '-' .substr($hash,20,12);
		return $guid;
		
		//$key = implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 30), 6));
	}
	function getTimeInISO()
	{
		/*Z - Timezone offset in seconds. The offset for timezones west of UTC is negative (-43200 to 50400)
		c - The ISO-8601 date (e.g. 2013-05-05T16:34:42+00:00)
		r - The RFC 2822 formatted date (e.g. Fri, 12 Apr 2013 12:01:05 +0200)
		U - The seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)*/
		return date("c");
	}
	
	function getAuthTokenHeader()
	{
		$token = null;
		$headers = apache_request_headers();
		if(isset($headers['Authorization']))
		{
			$matches = array();
			/*preg_match('/Token token="(.*)"/', $headers['Authorization'], $matches);
			if(isset($matches[1])){
				$token = $matches[1];
			}*/
			$arr = explode(" ",$headers['Authorization']);
			$token = $arr[1];
		}
		return $token;
	}
?>
