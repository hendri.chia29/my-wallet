<?php

if(!defined("BASEPATH")) exit ("No direct script access allowed");

class WalletHistoryModel extends CI_Model
{	private $table = "";

	public function __construct() {
    	parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
		$this->table = "history";
		$this->PK = "history_id";
		$this->PK2 = "customer_xid";
    }
	
	function getOneData($id)
	{
		if(!$id) return false;
		$sql = "select * from ".$this->table." where ". $this->PK ." ='".$id."'";
		$q = $this->db->query($sql, array($id));
		$data = array();
		if($q->num_rows() > 0)
		{
			 foreach($q->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$q->free_result();
		return $data;
	}
	function addData($data)
	{
		if(!is_array($data)) return false;
	 	$this->db->trans_start();
		$this->db->insert($this->table, $data);
        $newID = $this->db->insert_id();
		$this->db->trans_complete();
		return $this->db->trans_status();
	}
	
	function getTotalWallet($consumerId)
	{
		if(!$consumerId) return false;
		$sql = "select sum(amount) as ttl from ".$this->table." where ". $this->PK2 ." ='".$consumerId."' group by ".$this->PK2;
		$q = $this->db->query($sql);		
		$total = 0 ;
		if($q->num_rows() > 0)
		{
			$row = $q->row_array();
			$total = $row["ttl"];			
		}
		$q->free_result();
		return $total;
		
	}
}

?>