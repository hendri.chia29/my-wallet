<?php

if(!defined("BASEPATH")) exit ("No direct script access allowed");

class MsCustomerModel extends CI_Model
{	private $table = "";

	public function __construct() {
    	parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
		$this->table = "mscustomer";
		$this->PK = "customer_xid";
    }
	 
	function getData($datain)
	{
		$data = array();
		$sql = "select mk.*  from ".$this->table." mk  ";

		if($datain["id"]!="")
			$sql .=" where " .$this->PK. " =   '".trim($datain["id"]). "'";

		$q = $this->db->query($sql);

		if($q->num_rows() > 0)
		{
			
			//return $q;
			foreach($q->result_array() as $row)
			{
				$data[] = $row;
			}
			
			//$row = $q->row_array();
			//$data = $row;
		}
		$q->free_result();
		return $data;
	}
	
	function addData($data)
	{
		if(!is_array($data)) return false;
	 	$this->db->trans_start();
		$this->db->insert($this->table, $data);
        $newID = $this->db->insert_id();
		$this->db->trans_complete();
		return $this->db->trans_status();
	}
	function editData($data,$id)
	{
		if(!$id) return false;
		$this->db->update($this->table, $data,array($this->PK => $id));
		//die($this->db->last_query());
		return true;
	}
	function deleteData($id)
	{
		if(!$id) return false;
		$this->db->delete($this->table, array($this->PK => $id));
		return true;
	}
	function isDataExists($data)
	{
		$sql="select * from ".$this->table." where ".$this->PK."='".trim($data['id'])."'  ";
		$exists=0;
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			$exists=1;
		}
		$q->free_result();
		return $exists;
	}
}
?>